
import 'package:as2in/src/const/colors_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle getTextFormFieldStyle(){
  return TextStyle(
      fontFamily: 'Poppins-Regular',
      color: HexColor(COLOR_5F5F5F),
      fontWeight: FontWeight.w300,
      fontSize: 14
  );
}

TextStyle getTextFormFieldHintStyle(){
  return TextStyle(
      fontFamily: 'Poppins-Regular',
      color: HexColor(COLOR_5F5F5F),
      fontWeight: FontWeight.w300,
      fontSize: 10
  );
}

InputDecoration getInputDecoration(String label){
  return InputDecoration(
      labelText: label,
      labelStyle: getTextFormFieldStyle(),
      hintText: label,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: HexColor(COLOR_NON_ACTIVE)),
        //  when the TextFormField in unfocused
      ) ,
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color:HexColor(COLOR_NON_ACTIVE)),
        //  when the TextFormField in focused
      ) ,
      border: UnderlineInputBorder(
      )
  );
}
InputDecoration getInputDecorationSearchField(String label,bool isHasPrefix,isHasSuffix){
  return InputDecoration(
    border: InputBorder.none,
    hintText: label,
    filled: true,
    fillColor: HexColor(COLOR_5F5F5F),
    prefixIcon: isHasPrefix ? Icon(Icons.search) : null ,
    suffixIcon: isHasSuffix ? Icon(Icons.search) : null,
    contentPadding: const EdgeInsets.only(
        left: 14.0, bottom: 6.0, top: 8.0),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.transparent),
      borderRadius: BorderRadius.circular(10.0),
    ),
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.transparent),
      borderRadius: BorderRadius.circular(10.0),
    ),
  );
}
