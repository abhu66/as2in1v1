

class Validators{
  String validateFieldGeneral({String label, String value}) {
    if (value == null || value.isEmpty) return "$label is required";
    return null;
  }

  String validateFieldName(String value) {
    if (value == null || value.isEmpty) return "Nama tidak boleh kosong !";
    final validCharacters = RegExp(r'^[a-zA-Z ]+$');
    if(!validCharacters.hasMatch(value)) return "Nama tidak boleh mengandung spesial karakter \natau angka.";
    return null;
  }
  String validateFieldPhoneNumber(String value) {
    if (value == null || value.isEmpty) return "Nomor telepon tidak boleh kosong !";
    final validCharacters = RegExp(r'^[0-9]+$');
    if(!validCharacters.hasMatch(value)) return "Format nomor telepon salah.";
    return null;
  }

  String validateFieldAlamat(String value) {
    if (value == null || value.isEmpty) return "Alamat tidak boleh kosong !";
    final validCharacters = RegExp(r'^[a-zA-Z0-9 ^#\r\n]+$');
    if(!validCharacters.hasMatch(value)) return "Alamat tidak boleh mengandung spesial karakter!.";
    return null;
  }
  String validateFieldToken(String value) {
    if (value == null || value.isEmpty) return "Token tidak boleh kosong !";
    return null;
  }
  String validateFieldPassword(String value) {
    if (value == null || value.isEmpty) return "Password tidak boleh kosong !";
    return null;
  }
  String validateFieldEmail(String value) {
    if(value == null || value.isEmpty) return "Email tidak boleh kosong !";
    Pattern pattern =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if(!regex.hasMatch(value)) return "Format email salah (e.g. contoh@contoh.com)";
    return null;
  }
}