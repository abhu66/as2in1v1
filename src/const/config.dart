
import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'dart:developer';

enum StateChanged {LOADING,SUCCESS,FAILURE}
class ConfigNetwork {
  static Dio dio = Dio();
  static Response response;


  static Future<dynamic> doGet({String endpoint, String bearerToken}) async {
    Map headers = <String, String>{
      'Content-type': 'application/json',
      'Authorization': 'Bearer $bearerToken',
    };
    print("url : " + endpoint);
    try {
      response = await dio.get(endpoint, options: Options(
          headers: headers, contentType: Headers.formUrlEncodedContentType));
      log("response : ${response.toString()}");
      if (response.data['error'] != "")
        return throw new Exception(response.data['error'].toString());
      return response.data;
    } on DioError catch (e) {
      return throw new Exception(e.error);
    }
  }

 //post function with token
  static Future<dynamic> doPostWithToken(
      {String endpoint, String bearerToken, dynamic dataBody}) async {
      Map headers = <String, String>{
        'Content-type': 'application/json',
        'Authorization': 'Bearer $bearerToken',
      };
    var body = dataBody.toJson();
    try {
      response =
      await dio.post(endpoint, data: body, options: Options(headers: headers));
      if (response.data['error'] != "")
        return throw new Exception(response.data['error'].toString());
      return response.data;
    } on DioError catch (e) {
      return throw new Exception(e.message);
    }
  }

  //post function with token
  static Future<dynamic> doPostWithoutToken({String endpoint,dynamic dataBody,dynamic headers}) async {
    log("header : ${headers.toJson().toString()}");
    var header = headers.toJson();
    var body   = dataBody.toJson();
    log("Body : ${body.toString()}");
    log("endpoint : $endpoint");
    try {
      response = await dio.post(endpoint, data: body, options: Options(headers: header));
      log("res p : $response");
      if (response.data['code'] != "00" ) return throw new Exception(response.data['message'].toString());
      return response.data;
    } on DioError catch (e) {
      return throw new Exception(e.response.data['message'].toString().replaceAll("[", "").replaceAll("]", ""));
    }
  }

  //post function with token
  static Future<dynamic> doPostNoBody({String endpoint,dynamic headers}) async {
    var header = headers.toJson();
    try {
      response = await dio.post(endpoint,options: Options(headers: header));
      log("res p : $response");
      if (response.data['code'] != "00") return throw new Exception(response.data['message'].toString());
      return response.data;
    } on DioError catch (e) {
      return throw new Exception(e.response.data['message'].toString().replaceAll("[", "").replaceAll("]", ""));
    }
  }

  //post function with token
  static Future<dynamic> doGetWithToken({String endpoint,dynamic headers}) async {
    var header = headers.toJson();
    log("Header : ${header.toString()}");
    log("url : $endpoint");
    try {
      response = await dio.get(endpoint, options: Options(headers: header));
      log("res p : $response");
      if (response.data['code'] != "00") return throw new Exception(response.data['message'].toString());
      return response.data;
    } on DioError catch (e) {
      return throw new Exception(e.response.data['message'].toString().replaceAll("[", "").replaceAll("]", ""));
    }
  }

  //post function with token
  static Future<dynamic> doGetWithoutToken({String endpoint,dynamic headers}) async {
    var header = headers.toJson();
    log("Body : ${header.toString()}");
    try {
      response = await dio.get(endpoint, options: Options(headers: header));
      log("res p : $response");
      if (response.data['code'] != "00") return throw new Exception(response.data['message'].toString());
      return response.data;
    } on DioError catch (e) {
      return throw new Exception(e.response.data['message'].toString().replaceAll("[", "").replaceAll("]", ""));
    }
  }

}
