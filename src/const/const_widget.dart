
import 'package:as2in/src/const/colors_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:oktoast/oktoast.dart';

class ConstWidgets{
  static BuildContext _context;
  // ignore: non_constant_identifier_names
  static final double BTN_HEIGHT_DEFAULT = 48.0;
  // ignore: non_constant_identifier_names
  static final double BTN_WIDTH_DEFAULT = 100;

  static showAlert(String message,BuildContext context, Function onDismiss,bool isSuccess){
    ToastFuture toastFuture = showToastWidget(
      Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16.0),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                blurRadius: 500,
                spreadRadius: 500,
                color: Colors.grey.withOpacity(0.5),
              )
            ]
          ),
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.only(left: 30,right: 30),
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              isSuccess ? Icon(Icons.check_circle,size: 40,color: HexColor(COLOR_5F5F5F)) :
              Icon(Icons.error_outline_outlined,size: 40,color: HexColor(COLOR_5F5F5F),),
              SizedBox(height: 10,),
              Align(child :
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text: (message),
                        style:Theme.of(context).textTheme.headline1.copyWith(fontSize: 12.0)
                        )
                    ),
                  ),
                ],),
                alignment: Alignment.center,
              ),
            ],
          )
      ),
      duration: Duration(seconds: 3),
      onDismiss: onDismiss,
    );
  }

  static widgetError({BuildContext context,dynamic message}) {
    // Future.delayed(Duration.zero).then((value){
    //   ConstWidgets.showAlert(message, context, null, false);
    // });
    return Center(
      child: Container(
        child: Center(child : Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.info_rounded,size: 30,),
            SizedBox(height: 20,),
            RichText(
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              text: TextSpan(
                  text: "$message".replaceAll("Exception:", ""),
                  style: Theme.of(context).textTheme.subtitle1),
            ),
          ],
        ),
        ),
      ),
    );
  }

  static widgetNoConnection({BuildContext context,dynamic message}){
    return Container(
      child: Center(child : Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.wifi_off_rounded,size: 30,),
          SizedBox(height: 20,),
          RichText(
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
            text: TextSpan(
                text: "$message".replaceAll("Exception:", ""),
                style: Theme.of(context).textTheme.subtitle1),
          ),
        ],
      ),
      ),
    );
  }

  static doBack(BuildContext context){
    Future.delayed(Duration(seconds: 1)).then((_){
      Navigator.of(context).pop();
    });
  }
}

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.grey.withOpacity(0.5),
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  // shape:RoundedRectangleBorder(
                  //   borderRadius: BorderRadius.circular(16.0),
                  // ),
                  key: key,
                  children: <Widget>[
                    Center(
                        child:  Lottie.asset('assets/images/loader_lottie.json',height: 150,width: 150)
                    ),
                  ]));
        });
  }

  static Future<void> showLoadingDialogUploadProgress(
      BuildContext context, GlobalKey key,double _progressValue,int _progressPercentValue ) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: StatefulBuilder(
                    builder: (context, setState) {
                      return SimpleDialog(
                          shape:RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          key: key,
                          backgroundColor: HexColor(COLOR_TP_PRIMARY),
                          children: <Widget>[
                            Center(
                                child:  Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    new Container(
                                        padding: EdgeInsets.only(top: 10),
                                        child: new Column(children: <Widget>[
                                          Text(
                                            '$_progressPercentValue %',
                                            style:TextStyle(color: Colors.white)
                                          ),
                                        ])),
                                    Container(
                                        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                                        child: LinearProgressIndicator(value: _progressValue)),
                                  ],
                                )
                            ),
                          ]);
                    }),
          );
        });
  }
}