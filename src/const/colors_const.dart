
import 'package:flutter/cupertino.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    int colors;
    hexColor = hexColor.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    if (hexColor.length == 8) {
      colors = int.parse("0x$hexColor");
    }
    return colors;
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
//ALL COLOR DEFINED HERE
const COLOR_TP_PRIMARY      = "#c04c4c";
const COLOR_TP_SECONDARY    = "#FF6A0B";
const COLOR_NON_ACTIVE      = "#C4C4C4";
const COLOR_BUTTON_ROUNDED  = "#A0A0A0";
const COLOR_5F5F5F          = "#5F5F5F";
const COLOR_FFC50C          = "#FFC50C";
const COLOR_EDE2E2          = "#EDE2E2";
const COLOR_E8E8E8          = "#E8E8E8";
const COLOR_D33E5B          = "#D33E5B";
const COLOR_828282          = "#828282";
const COLOR_024D84          = "#024D84";
const COLOR_0167B1          = "#0167B1";
const COLOR_F1F1F1          = "#F1F1F1";
const COLOR_BG_LIGHT        = "#EAF1F5";
const COLOR_4F4F4F          = "#4F4F4F";


