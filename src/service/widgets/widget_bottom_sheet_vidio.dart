
import 'dart:async';
import 'dart:io';

import 'package:as2in/src/const/colors_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WidgetBottomSheetVidio extends StatefulWidget {
  final String url;
  const WidgetBottomSheetVidio({Key key,this.url}) : super(key: key);

  @override
  _WidgetBottomSheetVidioState createState() => _WidgetBottomSheetVidioState();
}

class _WidgetBottomSheetVidioState extends State<WidgetBottomSheetVidio> {
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  bool isLoading = true;
  int progress = 0;
  final Set<Factory> gestureRecognizers = [Factory(() => EagerGestureRecognizer()),].toSet();

  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          WebView(
            initialUrl: widget.url,
            gestureRecognizers: gestureRecognizers,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
            javascriptChannels: <JavascriptChannel>{
              _toasterJavascriptChannel(context),
            },
            onProgress: (int progress) {
              print("WebView is loading (progress : $progress%)");
              setState(() {
                this.progress = progress;
              });
            },
            onPageStarted: (String url) {
              print('Page started loading: $url');
            },
            onPageFinished: (String url) {
              print('Page finished loading: $url');
              setState(() {
                Future.delayed(Duration(milliseconds: 100));
                this.isLoading =  false;
              });
            },
            gestureNavigationEnabled: true,
          ),
          this.isLoading ?  Center(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: LinearPercentIndicator(
                width: MediaQuery.of(context).size.width - 50,
                animation: true,
                lineHeight: 20.0,
                animationDuration: 2500,
                percent: 1.0,
                center: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text:"$progress%",
                      style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 12, color: Colors.white)
                  ),
                ),
                linearStrokeCap: LinearStrokeCap.roundAll,
                progressColor: HexColor(COLOR_TP_PRIMARY),
              ),
            ),
          ) : Stack()
        ],
      ),
    );
  }
  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

}

class Choice {
  const Choice({this.title, this.icon});
  final String title;
  final String icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Customer Service', icon: "assets/images/service/customer_service_v3.png"),
  const Choice(title: 'Referral Program', icon: "assets/images/service/referal_program.png"),
  const Choice(title: 'Apply Referral', icon: "assets/images/service/apply_refereal_v2.png"),
  const Choice(title: 'ID Registration', icon: "assets/images/service/id_registration.png"),
  const Choice(title: 'Vidio', icon: "assets/images/service/vidio.png"),
  const Choice(title: 'INDOSOUND', icon: "assets/images/service/IndoSound.png"),
  const Choice(title: 'KlikDokter', icon: "assets/images/service/klik_dokter.png"),
  const Choice(title: 'Liputan 6', icon:"assets/images/service/liputan6_v2.png"),
  const Choice(title: 'Bola', icon:"assets/images/service/bola_v2.png"),
  const Choice(title: 'Bintang', icon: "assets/images/service/bintangcom.png"),
  const Choice(title: 'Liputan BMI', icon: "assets/images/service/logo.png"),
  const Choice(title: 'Apakabar', icon: "assets/images/service/logo_akol_circle.png"),
  const Choice(title: 'DDHK-Website', icon: "assets/images/service/DDHK_Website.png"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({Key key, this.choice}) : super(key: key);
  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.headline1.copyWith(fontSize: 12.0,);
    return Container(
      padding: EdgeInsets.all(5),
        child: Center(child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              Image.asset(choice.icon,width: 40,height: 40),
              SizedBox(height: 10,),
              Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
              SizedBox(height: 10,),
            ]
        ),
        )
    );
  }
}
