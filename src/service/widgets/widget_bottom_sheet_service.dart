import 'package:as2in/src/service/widgets/widget_bottom_sheet_vidio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetBottomSheetService extends StatefulWidget {
  const WidgetBottomSheetService({Key key}) : super(key: key);

  @override
  _WidgetBottomSheetServiceState createState() => _WidgetBottomSheetServiceState();
}

class _WidgetBottomSheetServiceState extends State<WidgetBottomSheetService> {
  @override
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return GridView.count(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.all(10),
        crossAxisCount: 4,
        crossAxisSpacing: 4.0,
        mainAxisSpacing: 8.0,
        children: List.generate(
            choices.length, (index) {
          return Center(
            child: SelectCard(choice: choices[index]),
          );
        }
        )
    );
  }
}

class Choice {
  const Choice({this.title, this.icon,this.url});
  final String title;
  final String icon;
  final String url;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Customer Service', icon: "assets/images/service/customer_service_v3.png",url: ""),
  const Choice(title: 'Referral Program', icon: "assets/images/service/referal_program.png",url: ""),
  const Choice(title: 'Apply Referral', icon: "assets/images/service/apply_refereal_v2.png",url: ""),
  const Choice(title: 'ID Registration', icon: "assets/images/service/id_registration.png",url: "https://regapps.kartuas2in1.com"),
  const Choice(title: 'Vidio', icon: "assets/images/service/vidio.png",url: "https://m.vidio.com"),
  const Choice(title: 'INDOSOUND', icon: "assets/images/service/IndoSound.png",url: "https://cast4.citrus3.com:2199/start/indosoundnetradio/"),
  const Choice(title: 'KlikDokter', icon: "assets/images/service/klik_dokter.png",url: "https://www.klikdokter.com/"),
  const Choice(title: 'Liputan 6', icon:"assets/images/service/liputan6_v2.png",url: "https://www.liputan6.com/"),
  const Choice(title: 'Bola', icon:"assets/images/service/bola_v2.png",url: "https://www.bola.com/"),
  const Choice(title: 'Bintang', icon: "assets/images/service/bintangcom.png",url: "https://www.fimela.com/"),
  const Choice(title: 'Liputan BMI', icon: "assets/images/service/logo.png",url: "https://www.liputanbmi.com/"),
  const Choice(title: 'Apakabar', icon: "assets/images/service/logo_akol_circle.png",url: "https://apakabaronline.com/"),
  const Choice(title: 'DDHK-Website', icon: "assets/images/service/DDHK_Website.png",url: "https://ddhk.org/"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({Key key, this.choice}) : super(key: key);
  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.headline1.copyWith(fontSize: 12.0,);
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(5),
          child: Center(child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Image.asset(choice.icon,width: 40,height: 40),
                SizedBox(height: 10,),
                Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
                SizedBox(height: 10,),
              ]
          ),
          )
      ),
      onTap: (){
          showModalBottomSheet(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              backgroundColor: Colors.white,
              isScrollControlled: true,
              context: context, builder: (BuildContext context){
            return Container(
                height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    InkWell(
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Spacer(),
                            RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                  text:choice.title,
                                  style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18)
                              ),
                            ),
                            Spacer(),
                            Icon(Icons.clear,size: 35,)
                          ],
                        ),
                        margin: EdgeInsets.all(20),
                      ),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    ),

                    WidgetBottomSheetVidio(url:choice.url),

                  ],
                ));
          }
          );
      },
    );
  }
}
