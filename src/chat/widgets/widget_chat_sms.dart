import 'package:as2in/src/chat/screens/chat_detail_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetchatSMSContainer extends StatefulWidget {
  final String querySearch;
  WidgetchatSMSContainer({this.querySearch});

  @override
  _WidgetchatSMSContainerState createState() => _WidgetchatSMSContainerState();
}

class _WidgetchatSMSContainerState extends State<WidgetchatSMSContainer> {

  List<ChatUsers> chatUsers = [
    ChatUsers(text: "Abu Khoerul Iskandar Ali", secondaryText: "Awesome Setup", imageURL: "images/userImage1.jpeg", time: "Now"),
    ChatUsers(text: "Mas Ray", secondaryText: "That's Great", imageURL: "images/userImage2.jpeg", time: "Yesterday"),
    ChatUsers(text: "Jorge Henry", secondaryText: "Hey where are you?", imageURL: "images/userImage3.jpeg", time: "31 Mar"),
    ChatUsers(text: "Philip Fox", secondaryText: "Busy! Call me in 20 mins", imageURL: "images/userImage4.jpeg", time: "28 Mar"),
    ChatUsers(text: "Debra Hawkins", secondaryText: "Thankyou, It's awesome", imageURL: "images/userImage5.jpeg", time: "23 Mar"),
    ChatUsers(text: "Jacob Pena", secondaryText: "will update you in evening", imageURL: "images/userImage6.jpeg", time: "17 Mar"),
    ChatUsers(text: "Andrey Jones", secondaryText: "Can you please share the file?", imageURL: "images/userImage7.jpeg", time: "24 Feb"),
    ChatUsers(text: "John Wick", secondaryText: "How are you?", imageURL: "images/userImage8.jpeg", time: "18 Feb"),
  ];

  List<ChatUsers> chatUserSearch = [];


  @override
  void initState(){
    super.initState();
    setState(() {
      chatUserSearch = chatUsers;
    });

  }

  @override
  void didUpdateWidget(covariant WidgetchatSMSContainer oldWidget) {
    // TODO: implement didUpdateWidget
    if(widget.querySearch != oldWidget.querySearch){
      setState(() {
        onSearchTextChanged(widget.querySearch);
      });

      print("aaa : ${widget.querySearch}");
    }

    super.didUpdateWidget(oldWidget);
  }

  onSearchTextChanged(String text) async {
    if (text.isEmpty) {
      return;
    }
    else {
      setState(() {
        chatUserSearch = chatUsers.where((chat) => chat.text.toLowerCase().contains(text.toLowerCase())).toList();
      });
    }

    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return listChat();
  }

  Widget listChat(){
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.only(top: 20),
        itemCount: chatUserSearch.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index){
          ChatUsers chatUser = chatUserSearch[index];
          return chatListItem(chatUsers: chatUser);
    });
  }

  Widget listChatSearch(){
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(top: 20),
        itemCount: chatUserSearch.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index){
          ChatUsers chatUser = chatUserSearch[index];
          return chatListItem(chatUsers: chatUser);
        });
  }

  Widget chatListItem({ChatUsers chatUsers}){
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context){
          return ChatDetailScreen(chatUsers: chatUsers,);
        }));
      },
      child: Container(
        padding: EdgeInsets.only(left: 16,right: 16,top: 10,bottom: 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.purpleAccent,
                    maxRadius: 20,
                  ),
                  SizedBox(width: 16,),
                  Expanded(
                    child: Container(
                      color: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(chatUsers.text, style: TextStyle(fontSize: 16),),
                          SizedBox(height: 6,),
                          Text(chatUsers.secondaryText,style: TextStyle(fontSize: 13,color: Colors.grey.shade600, fontWeight:FontWeight.normal),),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Text("${chatUsers.time}" ,style: TextStyle(fontSize: 12,fontWeight:FontWeight.normal),),
          ],
        ),
      ),
    );
  }
}

class ChatUsers{
  String text;
  String messageText;
  String imageURL;
  String secondaryText;
  String time;
  ChatUsers({@required this.text,@required this.messageText,@required this.imageURL,@required this.time,this.secondaryText});
}
