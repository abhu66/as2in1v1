import 'package:as2in/src/const/colors_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CallScreen extends StatefulWidget {

  @override
  _CallScreenState createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  TextEditingController _searchController = TextEditingController();
  bool visible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor("#FF0000"),
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.5,
        centerTitle: false,
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
              text:'Telephone',
              style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 25)
          ),
        ),
        backgroundColor: Colors.white,
        // actions: [
        //   // Expanded(child: serachField()),
        //   // Padding(
        //   //   padding: const EdgeInsets.only(right: 16),
        //   //   child: IconButton(icon: Icon(Icons.search,color: Colors.grey,size: 45,), onPressed: (){}),
        //   // ),
        // ],
      ),
      body:  Container(
          decoration: BoxDecoration(
            color: Colors.white,
            // image: DecorationImage(
            //   image: DateTime.now().hour >= 17  ? AssetImage(
            //       "assets/images/bg_night.jpg") : AssetImage(
            //       "assets/images/bg_day.png"),
            //   fit: BoxFit.cover,
            // ),
          ),
          child:ListView(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 10,left: 16,right: 16),
                child: Container(
                  height: 40,
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "Search...",
                      hintStyle: TextStyle(color: Colors.grey.shade600),
                      prefixIcon: Icon(Icons.search,color: Colors.grey.shade600, size: 20,),
                      filled: true,
                      fillColor: Colors.grey.shade100,
                      contentPadding: EdgeInsets.all(8),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                              color: Colors.grey.shade100
                          )
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                              color: Colors.grey.shade100
                          )
                      ),
                    ),
                  ),
                ),
              ),
              //serachField(),
              Visibility(
                visible: visible,
                child: Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 16,right: 26,top: 10),
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.greenAccent.withOpacity(0.5),
                      ),
                      child: ListTile(
                        dense: true,
                        title: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                              text:'Please make sure you have enough balance ',
                              style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14)
                          ),
                        ),
                        leading:  Icon(Icons.info_outline_rounded,size: 40,),
                        trailing: Container(
                          height: 30,
                          width: 80,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: Colors.red
                          ),
                          child: Center(
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                  text:'TOP UP',
                                  style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,color: Colors.white)
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                        left: 10,
                        child: InkWell(
                            onTap: (){
                              setState(() {
                                this.visible = false;
                              });
                            },
                            child: Icon(Icons.clear))),
                  ],
                ),
              ),
            ],
          )
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child : Image.asset("assets/images/ic_dial.png"),
      ),
    );
  }

  Widget serachField(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 40,
        margin: EdgeInsets.only(left: 10,right: 0),
        width: 350,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius:1,
              blurRadius: 0,
              offset: Offset(0, 0), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: widgetInputFeldApp(
              label: "",
              controller: _searchController,
              hint: "",
              textInputType: TextInputType.number,
              decoration: decoration(
                  hint:"Telephone",
                  label:"",
                  asset:""
              )
          ),
        ),
        padding: EdgeInsets.only(
            left: 20
        ),
      ),
    );
  }

  Widget widgetInputFeldApp({String label,String field, TextInputType textInputType,String hint, InputDecoration decoration,TextEditingController controller}) {
    return TextFormField(
      autofocus: true,
      controller: controller,
      cursorColor: HexColor(COLOR_NON_ACTIVE),
      style: TextStyle(
          fontFamily: 'Poppins-Regular',
          fontSize: 14,
          fontWeight: FontWeight.w300),
      keyboardType: textInputType,
      // validator: (value) {
      //   return Validators().validateFieldGeneral(label: label,value: value);
      // },
      onChanged: (value){
        setState((){
          if(this._searchController.value.text.isEmpty){

          }
          else{
          }

        });
      },
      decoration: decoration,
    );
  }

  InputDecoration decoration({String label,String hint,String asset}){
    return InputDecoration(
      labelText: label,
      labelStyle: TextStyle(
          fontFamily: 'Poppins-Regular',
          color: HexColor(COLOR_NON_ACTIVE),
          fontSize: 18,
          fontWeight: FontWeight.w300),
      hintText: hint,
      enabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      border: InputBorder.none,
      // prefixIcon: Padding(
      //     padding: EdgeInsets.only(right: 5, bottom: 0, top: 0),
      //     child: Image.asset(
      //       asset,
      //       scale: 4,
      //     )),
    );
  }
}
