import 'dart:async';
import 'dart:developer';
import 'package:as2in/src/const/colors_const.dart';
import 'package:as2in/src/main/screens/main_screen.dart';
import 'package:as2in/src/shared_prefs/shared_prefs.dart';
import 'package:as2in/src/splash/screen/images/const_images_splash.dart';
import 'package:as2in/src/started/screens/otp_screen.dart';
import 'package:as2in/src/started/screens/started_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  double opacityLevel = 0.0;
  TextEditingController _phoneNumberController = TextEditingController();
  @override
  void initState() {
    super.initState();
    //startSplashScreen();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Icon(Icons.arrow_back_rounded,color: Colors.white,),
        actions: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Icon(Icons.help_rounded,color: Colors.grey,),
          )
        ],
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.only(top: 0),
          physics: BouncingScrollPhysics(),
          children: [
            Stack(
              children: [
                Image.asset("assets/images/ic_slice_top.png",fit: BoxFit.cover,width: 250),

              ],
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 50,right: 50),
                child:Center(
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Let\'s get you started',
                            style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 20)
                        ),
                      ),
                      SizedBox(height: 10,),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Simply register your mobile number below',
                            style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,fontWeight: FontWeight.normal)
                        ),
                      ),
                      SizedBox(height: 30,),
                      Container(
                        height: 45,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                              bottomLeft: Radius.circular(40),
                              bottomRight: Radius.circular(40)
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.9),
                              spreadRadius:1.5,
                              blurRadius: 1.5,
                              offset: Offset(1, 0), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                          child: widgetInputFeldApp(
                              label: "",
                              controller: _phoneNumberController,
                              hint: "",
                              textInputType: TextInputType.number,
                              decoration: decoration(
                                  hint:"Input your phone number",
                                  label:"",
                                  asset:""
                              )
                          ),
                        ),
                        padding: EdgeInsets.only(
                            left: 20
                        ),
                      ),
                      SizedBox(height: 30,),
                      Image.asset("assets/images/ic_register_ava.png",height: 250,),
                      buttonNext(),
                      SizedBox(height: 20,),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'By Registering you are agreeing to \nour ',
                            style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,fontWeight: FontWeight.normal),
                            children: [
                              TextSpan(
                                text:'terms of service ',
                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,fontWeight: FontWeight.normal, color: Colors.red),
                              ),
                            ]
                        ),
                      ),
                    ],
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }

  goToOTPScreen() async {
    if(mounted)
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return OTPScreen();
      }),
    );
  }

  Widget widgetInputFeldApp({String label,String field, TextInputType textInputType,String hint, InputDecoration decoration,TextEditingController controller}) {
    return TextFormField(
      autofocus: true,
      controller: controller,
      cursorColor: HexColor(COLOR_NON_ACTIVE),
      style: TextStyle(
          fontFamily: 'Poppins-Regular',
          fontSize: 14,
          fontWeight: FontWeight.w300),
      keyboardType: textInputType,
      // validator: (value) {
      //   return Validators().validateFieldGeneral(label: label,value: value);
      // },
      onChanged: (value){
        setState((){
          if(this._phoneNumberController.value.text.isEmpty){

          }
          else{
          }

        });
      },
      decoration: decoration,
    );
  }

  InputDecoration decoration({String label,String hint,String asset}){
    return InputDecoration(
      labelText: label,
      labelStyle: TextStyle(
          fontFamily: 'Poppins-Regular',
          color: HexColor(COLOR_NON_ACTIVE),
          fontSize: 18,
          fontWeight: FontWeight.w300),
      hintText: hint,
      enabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      border: InputBorder.none,
      // prefixIcon: Padding(
      //     padding: EdgeInsets.only(right: 5, bottom: 0, top: 0),
      //     child: Image.asset(
      //       asset,
      //       scale: 4,
      //     )),
    );
  }

  Widget buttonNext(){
    return InkWell(
      onTap: (){
       goToOTPScreen();
      },
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: HexColor(COLOR_TP_PRIMARY),
        ),
        child: Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:'Register',
                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,color: Colors.white)
            ),
          ),
        ),
      ),
    );
  }
}
