import 'dart:async';
import 'package:as2in/src/const/colors_const.dart';
import 'package:as2in/src/main/screens/main_screen.dart';
import 'package:as2in/src/splash/screen/images/const_images_splash.dart';
import 'package:as2in/src/started/screens/faq_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OTPSuccessScreen extends StatefulWidget {
  @override
  _OTPSuccessScreenState createState() => _OTPSuccessScreenState();
}

class _OTPSuccessScreenState extends State<OTPSuccessScreen> {
  double opacityLevel = 0.0;
  @override
  void initState() {
    super.initState();
    //startSplashScreen();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Image.asset("assets/images/ic_slice_top.png",fit: BoxFit.cover,width: 250),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 50,right: 50),
              child:Center(
                child: ListView(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text:'OTP Success',
                          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 20)
                      ),
                    ),
                    SizedBox(height: 10,),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text:'Great news!, you\'re in',
                          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,fontWeight: FontWeight.normal)
                      ),
                    ),
                    Image.asset("assets/images/ic_otp_success.png",fit: BoxFit.cover),
                  ],
                ),
              ),
          ),
          Positioned(
              bottom: 30,
              right: 50,
              left: 50,
              child: buttonNext()),
        ],
      ),
    );
  }

  Widget buttonNext(){
    return InkWell(
      onTap: (){
        goToMinScreen();
      },
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: HexColor(COLOR_TP_PRIMARY),
        ),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end ,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    text:'Let\'s go',
                    style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,color: Colors.white)
                ),
              ),
              Spacer(),
              Icon(Icons.arrow_forward_rounded,color: Colors.white,),
              SizedBox(width: 10,),
            ],
          ),
        ),
      ),
    );
  }

  goToMinScreen() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return MainScreen();
      }),
    );
  }
}
