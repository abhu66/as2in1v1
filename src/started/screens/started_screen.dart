import 'dart:async';
import 'package:as2in/src/const/colors_const.dart';
import 'package:as2in/src/main/screens/main_screen.dart';
import 'package:as2in/src/splash/screen/images/const_images_splash.dart';
import 'package:as2in/src/started/screens/faq_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StartedScreen extends StatefulWidget {
  @override
  _StartedScreenState createState() => _StartedScreenState();
}

class _StartedScreenState extends State<StartedScreen> {
  double opacityLevel = 0.0;
  @override
  void initState() {
    super.initState();
    //startSplashScreen();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Image.asset("assets/images/ic_slice_top.png",fit: BoxFit.cover,width: 250),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 50,right: 50),
              child:Center(
                child: ListView(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    Image.asset(ConstImages.logoSplash,fit: BoxFit.cover),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text:'Welcome to As2in1',
                          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 20)
                      ),
                    ),
                    SizedBox(height: 10,),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text:'Ready to enjoy chat, media and balance transfers with your family and friends in indonesia?',
                          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,fontWeight: FontWeight.normal)
                      ),
                    ),
                  ],
                ),
              ),
          ),
          Positioned(
              bottom: 30,
              right: 50,
              left: 50,
              child: buttonNext()),
        ],
      ),
    );
  }

  Widget buttonNext(){
    return InkWell(
      onTap: (){
        goToFAQ();
      },
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: HexColor(COLOR_TP_PRIMARY),
        ),
        child: Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:'Get Started',
                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,color: Colors.white)
            ),
          ),
        ),
      ),
    );
  }



  startSplashScreen() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration,() async {
      goToFAQ();
    });
  }

  goToFAQ() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return FAQScreen();
      }),
    );
  }
}
