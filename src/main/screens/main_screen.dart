

import 'dart:developer';

import 'package:as2in/src/account/screens/account_screen.dart';
import 'package:as2in/src/account/screens/profile_screen.dart';
import 'package:as2in/src/call/screen/call_screen.dart';
import 'package:as2in/src/chat/screens/chat_screen.dart';
import 'package:as2in/src/const/colors_const.dart';
import 'package:as2in/src/contacts/screens/contact_screens.dart';
import 'package:as2in/src/generated/user.pb.dart';
import 'package:as2in/src/home/screens/home_screen.dart';
import 'package:as2in/src/user/service/user_service.dart';
import 'package:badges/badges.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';


class MainScreen extends StatefulWidget {
  final tabSelected;
  MainScreen({this.tabSelected});
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;
  bool isCartScreenFinal = false;
  int badge     = 0;
  int badgeChat = 0;
  bool isTap = false;

  @override
  void initState(){
    super.initState();
   // _getPermission();
    UserService.getUser(id: "11181").then((value) {
      User user = value;
      log("User : ${user.userid}");
    }).catchError((err){
      log("error :$err");
    });
    this._selectedIndex = widget.tabSelected ?? 0;
  }



  @override
  Widget build(BuildContext context) {

    List<Widget> widgets = [
      HomeScreen(),
      CallScreen(),
      ChatScreen(),
      ContactsScreen(),
      ProfileScreen(),

    ];

    return Scaffold(
      body:widgets[_selectedIndex],
      bottomNavigationBar: BottomNavyBar(
        backgroundColor: Colors.white,
        onItemSelected: _onItemTapped,
        selectedIndex: _selectedIndex,
        showElevation: true,
        itemCornerRadius: 24,
        curve: Curves.bounceInOut,
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            icon: Container(
              child: Image.asset("assets/ic_home.png",width: 25,height: 25,color:_selectedIndex == 0 ? Colors.red : Colors.grey,),
            ),
            title: Text('As2in1'),
            activeColor: Colors.red,
            textAlign: TextAlign.center,
            inactiveColor: Colors.grey,
            // icon: Container(
            //   child: Icon(Icons.home_rounded,size:30,color: _selectedIndex == 0 ? HexColor(COLOR_TP_PRIMARY)  :Colors.grey),
            // ),
            // label: 'As2in1',
          ),
          BottomNavyBarItem(
            icon: Badge(
              elevation: 0.0,
              badgeColor:this.badgeChat == 0 ? Colors.transparent : HexColor(COLOR_FFC50C),
              shape: BadgeShape.circle,
              borderRadius: BorderRadius.circular(100),
              child: Image.asset("assets/ic_call.png",width: 25,height: 25,color:_selectedIndex == 1 ? Colors.purpleAccent : Colors.grey),
              badgeContent: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text:'${this.badgeChat}',
                  style: TextStyle(
                      color:this.badgeChat == 0 ? Colors.transparent : Colors.grey,
                      fontSize: 10,
                      fontFamily: "Poppins-Black",
                      fontWeight: FontWeight.w400),
                ),
              ),
            ),
            title: Text('Calls'),
            activeColor: Colors.purpleAccent,
            textAlign: TextAlign.center,
            inactiveColor: Colors.grey,
            // icon: Container(
            //   child: Image.asset(ConstImages.icChat,scale: 5,color: _selectedIndex == 1 ? HexColor(COLOR_TP_PRIMARY) : Colors.black),
            // ),
            //label: 'Calls',
          ),
          BottomNavyBarItem(
            icon: Container(
              child:Image.asset("assets/ic_chat.png",width: 25,height: 25,color:_selectedIndex == 2 ? Colors.pinkAccent : Colors.grey),
            ),
            title: Text('Chat'),
            activeColor: Colors.pinkAccent,
            textAlign: TextAlign.center,
            inactiveColor: Colors.grey,

            // label: 'Chat',
          ),
          BottomNavyBarItem(
            icon: Container(
              child:Image.asset("assets/ic_contacts.png",width: 25,height: 25,color:_selectedIndex == 3 ? Colors.pink : Colors.grey),
            ),
            title: Text('Contacts'),
            activeColor: Colors.pink,
            textAlign: TextAlign.center,
            inactiveColor: Colors.grey,

            // label: 'Chat',
          ),
          BottomNavyBarItem(
            icon: Container(
              child: Image.asset("assets/ic_user.png",width: 25,height: 25,color:_selectedIndex == 4 ? Colors.blue : Colors.grey),
            ),
            title: Text('Account'),
            activeColor: Colors.blue,
            inactiveColor: Colors.grey,
            textAlign: TextAlign.center,
            //label: 'Account',
          ),
        ],

        // currentIndex: _selectedIndex,
        // selectedItemColor: Colors.red,
        // // showSelectedLabels: false,
        // // showUnselectedLabels: false,
        // onTap: _onItemTapped,
      ),
    );
  }



  void _onItemTapped(int index) {
    setState(() {
      this.isTap = !this.isTap;
      _selectedIndex = index;
      if(index == 1){
        //NavigatorScreen.goToWhere(menu: "/chat",context: context);
      }
    });
  }

  countBadge(int badge){
    setState(() {
      print("badge : $badge");
      this.isTap = !this.isTap;
    });
  }

  countBadgeChat(int badge){
    setState(() {
      this.badgeChat = badge;
    });
  }

  //Check contacts permission
  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
      await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ?? PermissionStatus.restricted;
    } else {
      return permission;
    }
  }
}
