import 'dart:developer';

import 'package:as2in/src/const/colors_const.dart';
import 'package:as2in/src/generated/user.pb.dart';
import 'package:as2in/src/generated/user.pbgrpc.dart';
import 'package:as2in/src/grpc/grpc_commons.dart';
import 'package:as2in/src/user/service/user_service.dart';
import 'package:dropdown_banner/dropdown_banner.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

abstract class UserContract{
  void onStateChanged(StateChange stateChange);
}

class UserPresenter{
  UserContract view;
  static User userResponse;
  static String errorMessage;

  static doGetUser({String userId,UserContract view}) async {
    view.onStateChanged(StateChange.LOADING);
      UserService.getUser(id: userId).then((value) {
          userResponse = value;
          log("user : $value");
          view.onStateChanged(StateChange.SUCCESS);
        }).catchError((error){
          view.onStateChanged(StateChange.FAILURE);
          errorMessage = error.toString();
          DropdownBanner.showBanner(duration: Duration(seconds: 3),
            text: 'Ops... something went wrong. [${error.code}]',
            color: Colors.redAccent,
            textStyle: TextStyle(color: Colors.white,fontSize: 18),
          );
          log("error message : $errorMessage");
      });
  }
}

class GetUser{
  CallBack method;
  GetUser({this.method});
}

typedef CallBack = void Function({String userId,UserContract view});