import 'package:as2in/src/generated/user.pb.dart';
import 'package:as2in/src/generated/user.pbgrpc.dart';
import 'package:as2in/src/grpc/grpc_commons.dart';

class UserService {

  static Future<User> getUser({String id}) async{
    var client = UserServiceClient(GrpcClientSingleton().client);
    return await client.getUser(User(userid: id));
  }

  static Future<List<User>> getAllUser(){
    var client = UserServiceClient(GrpcClientSingleton().client);
    return client.all(AllRequest()).cast<User>().toList();
  }
}