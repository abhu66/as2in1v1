import 'package:as2in/src/purchase/widgets/widget_bottom_sheet_transfer_balance.dart';
import 'package:as2in/src/purchase/widgets/widget_bottom_sheet_transfer_data_package.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetBottomSheetTopUp extends StatefulWidget {
  const WidgetBottomSheetTopUp({Key key}) : super(key: key);

  @override
  _WidgetBottomSheetTopUpState createState() => _WidgetBottomSheetTopUpState();
}

class _WidgetBottomSheetTopUpState extends State<WidgetBottomSheetTopUp> {
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return GridView.count(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.all(10),
        crossAxisCount: 4,
        crossAxisSpacing: 4.0,
        mainAxisSpacing: 8.0,
        children: List.generate(
            choices.length, (index) {
          return Center(
            child: SelectCard(choice: choices[index],index: index,),
          );
        }
        )
    );
  }
}

class Choice {
  const Choice({this.title, this.icon});
  final String title;
  final String icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Paper Voucher', icon: "assets/images/topup/ic_paper_voucher.png"),
  const Choice(title: 'Circle-K Top Up', icon: "assets/images/topup/ic_ck.png"),
  const Choice(title: 'Indonesia Top Up', icon: "assets/images/topup/ic_id_topup.png"),
  const Choice(title: 'Taiwan Top Up', icon: "assets/images/topup/ic_taiwan_topup.png"),
  const Choice(title: 'PayPal Top Up', icon: "assets/images/topup/ic_paypal.png"),
  const Choice(title: 'TNG Top Up', icon: "assets/images/topup/ic_tng.png"),
  const Choice(title: 'Saudi Pinless Top Up', icon: "assets/images/topup/ic_saudi.png"),
  const Choice(title: 'SRS Malaysia Top Up', icon:"assets/images/topup/ic_srs_malay.png"),
  const Choice(title: 'Malaysia MyGraPARI Top Up', icon:"assets/images/topup/ic_malay_grapari.png"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({Key key, this.choice,this.index}) : super(key: key);
  final Choice choice;
  final int index;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.headline1.copyWith(fontSize: 12.0,);
    return InkWell(
      child: Container(
          padding: EdgeInsets.all(5),
          child: Center(child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Image.asset(choice.icon,width: 40,height: 40),
                SizedBox(height: 10,),
                Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
                SizedBox(height: 10,),
              ]
          ),
          )
      ),
      onTap: (){
        showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30)
              ),
            ),
            backgroundColor: Colors.white,
            isScrollControlled: true,
            context: context, builder: (BuildContext context){
          return Container(
              height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  InkWell(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Spacer(),
                          RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text:choice.title,
                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18)
                            ),
                          ),
                          Spacer(),
                          Icon(Icons.clear,size: 35,)
                        ],
                      ),
                      margin: EdgeInsets.all(20),
                    ),
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                  ),
                  setWidgetScreen(index: index)
                ],
              ));
        }
        );
      },
    );
  }

  Widget setWidgetScreen({int index}){
    switch(index){
      case 0 :
        return   WidgetBottomSheetTransferBalance(); break;
      case 1 :
        return   WidgetBottomSheetTransferDataPackage(); break;
      default : break;
    }
  }
}
