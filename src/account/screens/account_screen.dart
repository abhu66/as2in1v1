import 'dart:developer';

import 'package:as2in/src/const/colors_const.dart';
import 'package:as2in/src/generated/user.pb.dart';
import 'package:as2in/src/grpc/grpc_commons.dart';
import 'package:as2in/src/user/presenter/user_presenter.dart';
import 'package:as2in/src/user/service/user_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key key}) : super(key: key);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> implements UserContract {
   User user;
   List<User> listUser = [];

  @override
  void initState(){
    super.initState();
    UserPresenter.doGetUser(userId: "11111",view: this);
    UserService.getAllUser().then((value){
      setState(() {
        this.listUser.addAll(value);
        log("asa : "+this.listUser.length.toString());
        listUser.map((e) {
          log("e : "+e.toString());
        }).toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor(COLOR_TP_PRIMARY),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: HexColor(COLOR_TP_PRIMARY),
        actions: [
          IconButton(icon: Icon(Icons.notifications,color: Colors.white,), onPressed: (){}),
          IconButton(icon: Icon(Icons.help,color: Colors.white,), onPressed: (){}),
          IconButton(icon: Icon(Icons.share_rounded,color: Colors.white,), onPressed: (){})
        ],
      ),
      body:  Container(
          child:ListView(
            physics: BouncingScrollPhysics(),
            children: [
                  Wrap(
                    children: [
                      Center(
                        child: CircleAvatar(
                          radius: 50,
                          backgroundColor: Colors.transparent,
                          child: Icon(Icons.account_circle_outlined,size: 79,),
                        ),
                      ),
                      Center(child: widgetBalance()),
                    ],
                  ),
                  SizedBox(width: 20,),
                  widgetDashboard(),
                ],
              ),
          )
    );
  }

  Widget widgetBalance(){
    return Container(
      child: ListTile(
        dense: true,
        title: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text:'${UserPresenter.userResponse?.firstname ?? "Your Full"} ${UserPresenter.userResponse?.lastname ?? "Name"}',
              style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white,fontSize: 16)
          ),
        ),
        subtitle: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text:"Register : +${UserPresenter.userResponse?.phone ?? "+62xxxxxxxxx"} \n",
              style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white,fontSize: 14,fontWeight: FontWeight.normal),
            children: [
              TextSpan(
                text:"As2in1 App : +xxxxxxxxx",
                style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white,fontSize: 14,fontWeight: FontWeight.normal)
                )
              ]
          ),
        ),
      ),
    );
  }

  Widget widgetTotalBalance(){
    return Container(
      child: ListTile(
        title: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text:"\$",
              style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white,fontSize: 25),
              children: [
                TextSpan(
                  text:"2.00",
                  style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white),
                ),
                TextSpan(
                  text:" USD",
                  style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white,fontSize: 14,fontWeight: FontWeight.normal),
                )
              ]
          ),
        ),
      ),
    );
  }

  Widget widgetDashboard(){
    return Container(
      margin: EdgeInsets.only(top: 50),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          )
      ),
      child: ListView(
        padding: EdgeInsets.only(left: 20,top: 20,right: 20,bottom: 30),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          widgetUtil(),
          SizedBox(height: 10,),
          RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text:'SETTINGS',
                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18,fontWeight: FontWeight.w600)
            ),
          ),
          SizedBox(height: 10,),
          widgetListMenuSetting(),
          Divider(),
          SizedBox(height: 16,),
          RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text:'APPLICATION INFROMATION',
                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18,fontWeight: FontWeight.w600)
            ),
          ),
          SizedBox(height: 10,),
          widgetListMenuInformation(),
          SizedBox(height: 40,),
        ],
      ),
    );
  }

  Widget widgetListMenuSetting(){
    return ListView.separated(
      shrinkWrap: true,
      itemCount: menuSetting.length,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          contentPadding: EdgeInsets.zero,
          dense: true,
          title: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: menuSetting[index],
                style: Theme
                    .of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 14, fontWeight: FontWeight.w400)
            ),
          ),
          trailing: Icon(Icons.arrow_forward_ios_rounded, color: Colors.grey,),
          onTap: (){

          },
        );
      },
      separatorBuilder: (BuildContext context, int index){
        return Divider();
      },
    );
  }

  Widget widgetListMenuInformation(){
    return ListView.separated(
      shrinkWrap: true,
      itemCount: menuInformation.length,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          contentPadding: EdgeInsets.zero,
          dense: true,
          title: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: menuInformation[index],
                style: Theme
                    .of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 14, fontWeight: FontWeight.w400)
            ),
          ),
          trailing: Icon(Icons.arrow_forward_ios_rounded, color: Colors.grey,),
          onTap: (){

          },
        );
      },
      separatorBuilder: (BuildContext context, int index){
        return Divider();
      },
    );
  }

  Widget widgetUtil(){
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(
        top: 10,
        bottom: 30,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.grey.withOpacity(0.5)),
            color: Colors.white,

        ),
        height: 80,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              child: Container(
                height: 100,
                width: 100,
                child:Center(
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Image.asset("assets/images/purchase/balance_transfer_v2.png",width: 30,height: 30,),
                      SizedBox(height: 5,),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Top-Up',
                            style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,)
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              onTap: (){
                showModalBottomSheet(

                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    backgroundColor: Colors.white,
                    isScrollControlled: true,
                    context: context, builder: (BuildContext context){
                  return Container(
                      height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          InkWell(
                            child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                        text:'Top Up Option',
                                        style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18)
                                    ),
                                  ),
                                  Icon(Icons.keyboard_arrow_down_rounded,size: 35,)
                                ],
                              ),
                              margin: EdgeInsets.all(20),
                            ),
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                          ),
                          //WidgetBottomSheetTopUp(),
                        ],
                      ));
                }
                );
              },
            ),
            SizedBox(width: 10,),
            InkWell(
              child: Container(
                height: 100,
                width: 100,
                // decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(10),
                //     color: Colors.white,
                //     boxShadow: [
                //       BoxShadow(
                //           color: Colors.grey,
                //           blurRadius: 30,
                //           spreadRadius: 1,
                //           offset: Offset(0,0)
                //       )
                //     ]
                // ),
                child:Center(
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Image.asset("assets/images/service/apply_refereal.png",width: 30,height: 30,),
                      SizedBox(height: 5,),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Apply Referal',
                            style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14)
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              onTap: (){
                showModalBottomSheet(

                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    backgroundColor: Colors.white,
                    isScrollControlled: true,
                    context: context, builder: (BuildContext context){
                  return Container(
                      height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          InkWell(
                            child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                        text:'Apply Referal',
                                        style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18)
                                    ),
                                  ),
                                  Icon(Icons.keyboard_arrow_down_rounded,size: 35,)
                                ],
                              ),
                              margin: EdgeInsets.all(20),
                            ),
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                          ),
                          //WidgetBottomSheetPurchase(),
                        ],
                      ));
                }
                );
              },
            ),
            SizedBox(width: 10,),
            InkWell(
              child: Container(
                height: 100,
                width: 100,
                // decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(10),
                //     color: Colors.white,
                //     boxShadow: [
                //       BoxShadow(
                //           color: Colors.grey,
                //           blurRadius: 30,
                //           spreadRadius: 1,
                //           offset: Offset(0,0)
                //       )
                //     ]
                // ),
                child:Center(
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Image.asset("assets/images/service/referal_program.png",width: 30,height: 30,),
                      SizedBox(height: 5,),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Referal Program',
                            style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14)
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              onTap: (){
                showModalBottomSheet(

                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    backgroundColor: Colors.white,
                    isScrollControlled: true,
                    context: context, builder: (BuildContext context){
                  return Container(
                      height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          InkWell(
                            child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                        text:'Service',
                                        style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18)
                                    ),
                                  ),
                                  Icon(Icons.keyboard_arrow_down_rounded,size: 35,)
                                ],
                              ),
                              margin: EdgeInsets.all(20),
                            ),
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                          ),
                          //WidgetBottomSheetService(),
                        ],
                      ));
                }
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  void onStateChanged(StateChange stateChange) {
    if(mounted)
    setState(() {
      switch(stateChange){
        case StateChange.LOADING : break;
        case StateChange.FAILURE :break;

        case StateChange.SUCCESS : break;
      }
    });
  }
}

const List<String> menuSetting = [
  "Language",
  "Currency",
  "Pricing",
  "Phone",
  "Chat",
  "Re-download Phone Book"
];

const List<String> menuInformation = [
  "Customer Service",
  "Send us the Trace Log",
  "Test Call",
  "Terms and Conditions & Privacy Policy",
  "Exit Application",
];