import 'package:as2in/src/purchase/widgets/widget_bottom_sheet_purchase.dart';
import 'package:as2in/src/service/widgets/widget_bottom_sheet_service.dart';
import 'package:as2in/src/service/widgets/widget_bottom_sheet_vidio.dart';
import 'package:as2in/src/topup/widgets/widget_bottom_sheet_topup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetProfile extends StatefulWidget {
  const WidgetProfile({Key key}) : super(key: key);

  @override
  _WidgetProfileState createState() => _WidgetProfileState();
}

class _WidgetProfileState extends State<WidgetProfile> {
  @override
  Widget build(BuildContext context) {
    return widgetPopular();
  }

  Widget widgetPopular(){
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20)
      ),
      margin: EdgeInsets.only(
        top: 20,
        left: 10,
        right: 10
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset("assets/images/ic_as_red.png",width: 35),
              ),
             Spacer(),
              Center(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text:'+62 85231835980',
                      style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14)
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset("assets/images/ic_as_red.png",width: 35,color: Colors.transparent,),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                      text:"\$",
                      style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.black,fontSize: 20),
                      children: [
                        TextSpan(
                          text:"2.00",
                          style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.black,fontSize: 20),
                        ),
                        TextSpan(
                          text:" USD",
                          style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.black,fontSize: 14,fontWeight: FontWeight.normal),
                        )
                      ]
                  ),
                ),

                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text:'Active until 29 Aug 2021',
                      style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,fontWeight: FontWeight.normal)
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              widgetMenu(menuName: "Top-Up"),
              widgetMenu(menuName: "Purchase"),
            ],
          ),

          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              widgetMenu(menuName: "Service"),
              widgetMenu(menuName: "Voice & SMS"),
            ],
          ),
          SizedBox(height: 10,),
        ],
      ),
    );
  }


  Widget widgetMenu({String menuName}){
    return InkWell(
      child: Container(
        height: 38,
        width: 163,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurRadius: 1,
                  spreadRadius: 0,
                  offset: Offset(0,0)
              )
            ]
        ),
        child:Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:'$menuName',
                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14)
            ),
          ),
        ),
      ),
      onTap: (){
        showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30)
              ),
            ),
            backgroundColor: Colors.white,
            isScrollControlled: true,
            context: context, builder: (BuildContext context){
              return Container(
                  height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      InkWell(
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    text:'$menuName Option',
                                    style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18)
                                ),
                              ),
                              Icon(Icons.keyboard_arrow_down_rounded,size: 35,)
                            ],
                          ),
                          margin: EdgeInsets.all(20),
                        ),
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                      ),
                      menuName == "Service" ?
                      WidgetBottomSheetService() : menuName == "Purchase" ?
                      WidgetBottomSheetPurchase() : menuName == "Top-Up" ?
                      WidgetBottomSheetTopUp() : Container(),
                    ],
              ));
            }
        );
      },
    );
  }
}
