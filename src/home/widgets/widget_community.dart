import 'package:as2in/src/home/widgets/widget_cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetCommunity extends StatefulWidget {
  const WidgetCommunity({Key key}) : super(key: key);

  @override
  _WidgetCommunityState createState() => _WidgetCommunityState();
}

class _WidgetCommunityState extends State<WidgetCommunity> {
  @override
  Widget build(BuildContext context) {
    return widgetPopular();
  }

  Widget widgetPopular(){
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(
        top: 40,
        left: 20,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text:'Community',
                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14)
            ),
          ),
          SizedBox(height: 10,),
          Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                      width: 150,

                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,

                      ),
                      child:WidgetCacheNetworkImageCommunity(
                        radius: 20,
                        urlImage: "https://ebooks.gramedia.com/ebook-covers/45586/image_highres/ID_NGI2019MTH04ED04.jpg",
                      )
                    ),
                    SizedBox(width: 16,),
                    Container(
                      height: 50,
                      width: 150,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,

                      ),
                        child:WidgetCacheNetworkImageCommunity(
                          radius: 20,
                          urlImage: "https://ebooks.gramedia.com/ebook-covers/45586/image_highres/ID_NGI2019MTH04ED04.jpg",
                        )
                    ),
                    SizedBox(width: 16,),
                    Container(
                      height: 50,
                      width: 150,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                      ),
                        child:WidgetCacheNetworkImageCommunity(
                          radius: 20,
                          urlImage: "https://ebooks.gramedia.com/ebook-covers/45586/image_highres/ID_NGI2019MTH04ED04.jpg",
                        )
                    ),
                    SizedBox(width: 16,),
                  ],
                ),
              ),
            ],
          ),

          SizedBox(height: 20,),
          Container(
              width: MediaQuery.of(context).size.width,
              height: 150,
              margin: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,

              ),
              child:WidgetCacheNetworkImageCommunity(
                radius: 20,
                urlImage: "https://ebooks.gramedia.com/ebook-covers/45586/image_highres/ID_NGI2019MTH04ED04.jpg",
              )
          ),
        ],
      ),
    );
  }
}
