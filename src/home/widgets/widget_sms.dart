import 'package:as2in/src/const/colors_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetSMS extends StatefulWidget {
  @override
  _WidgetSMSState createState() => _WidgetSMSState();
}

class _WidgetSMSState extends State<WidgetSMS> {

  List<String> listSMS = [
    "1",
    "2",
    "3",
    "4"
  ];

  @override
  Widget build(BuildContext context) {
    return listSMS.length == 0 ? Container() : Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(20)),
      margin: EdgeInsets.only(top: 20, left: 10, right: 10),
      child: ListView(
        padding: EdgeInsets.only(top: 0),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Container(
            margin: EdgeInsets.only(right: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                      text: 'SMS',
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontSize: 20)),
                ),
              ],
            ),
          ),
          ListView.builder(
            padding: EdgeInsets.only(top: 0),
              itemCount: listSMS.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index){
                return  Dismissible(
                     key: UniqueKey(),
                     onDismissed: (value){
                      setState(() {
                        this.listSMS.removeAt(index);
                      });
                     },
                    child: widgetListSMS()
                );
          }),

        ],
      ),
    );
  }


  Widget widgetListSMS(){
    return Container(
        margin: EdgeInsets.only(top: 10.0),
        padding: EdgeInsets.only(right: 16.0),
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: LinearGradient(
            colors: [HexColor("#BBBBBB"), HexColor("#E4E6E8")],
          ),
        ),
        width: MediaQuery.of(context).size.width,
        child: ListTile(
          dense: true,
          contentPadding: EdgeInsets.zero,
          leading: Container(
              margin: EdgeInsets.only(
                left: 10.0,
              ),
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: HexColor("#5FC5FF"),
              ),
              child: Center(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text: 'AK',
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontSize: 18, color: Colors.white)),
                ),
              )),
          title: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: 'Abu Khoerul Iskandar Ali',
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 16, color: Colors.black)),
          ),
          subtitle: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: 'Lorem ipsum dolor sit amet c...',
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 12, color: Colors.black)),
          ),
          trailing: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: '14:59',
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 12, color: Colors.black)),
          ),
        ));
  }
}
