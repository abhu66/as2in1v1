import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetCacheNetworkImageCommunity extends StatefulWidget {
  final String urlImage;
  final double height;
  final double radius;
  WidgetCacheNetworkImageCommunity({this.urlImage,this.height,this.radius});
  @override
  _WidgetCacheNetworkImageCommunityState createState() => _WidgetCacheNetworkImageCommunityState();
}

class _WidgetCacheNetworkImageCommunityState extends State<WidgetCacheNetworkImageCommunity>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CachedNetworkImage(
      imageUrl: widget.urlImage,
      imageBuilder:
          (context, imageProvider) =>
          Container(
            height: widget.height ?? 288,
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.all(Radius.circular(widget.radius ?? 0),
              ),
              image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover),
            ),
          ),
      placeholder: (context, url) =>
          Container(
            height:widget.height ?? 288,
            child : Center(child :  Container(
                height: 25,width: 25,
                child : CircularProgressIndicator(strokeWidth: 1,))),),
      errorWidget: (context, url, error)
      => Container(
        height: widget.height ?? 288,
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.all(Radius.circular(widget.radius ?? 0),
          ),
          image: DecorationImage(
              image:AssetImage("assets/images/tasya_banner_1.png"),
              fit: BoxFit.cover),
        ),
      ),
    );
  }
}
