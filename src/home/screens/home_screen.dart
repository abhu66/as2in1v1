import 'package:as2in/src/const/colors_const.dart';
import 'package:as2in/src/home/widgets/widget_popular.dart';
import 'package:as2in/src/home/widgets/widget_recomendation.dart';
import 'package:as2in/src/home/widgets/widget_sms.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: HexColor("#FF0000"),
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        actions: [
          IconButton(icon: Icon(Icons.notifications,color: Colors.white,), onPressed: (){}),
          IconButton(icon: Icon(Icons.help,color: Colors.white,), onPressed: (){})
        ],
      ),
      body:  Container(
        decoration: BoxDecoration(
          color: HexColor("#E5E5E5"),
          image: DecorationImage(
            image: DateTime.now().hour >= 17  ? AssetImage(
               "assets/images/bg_night.jpg") : AssetImage(
                "assets/images/bg_day.png"),
            fit: BoxFit.cover,
          ),
        ),
          child:ListView(
            padding: EdgeInsets.only(bottom: 0,top: 100),
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(width: 16,),
                  Icon(Icons.account_circle_outlined,color: Colors.white,size: 70,),
                  SizedBox(width: 5,),
                  Flexible(
                    child: Wrap(
                      children: [
                        widgetBalance(),
                        //widgetTotalBalance()
                      ],
                    ),
                  ),
                ],
              ),
              widgetDashboard(),
            ],
          )
      ),
    );
  }

  Widget widgetBalance(){
    return Container(
      child: ListTile(
        dense: true,
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
              text:greater(),
              style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white)
          ),
        ),
        subtitle: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
              text:"Abu Khoerul Iskandar Ali",
              style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white,fontSize: 16,fontWeight: FontWeight.normal)
          ),
        ),
      ),
    );
  }

  Widget widgetTotalBalance(){
    return Container(
      child: ListTile(
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
              text:"\$",
              style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white,fontSize: 25),
              children: [
                TextSpan(
                  text:"2.00",
                  style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white),
                ),
                TextSpan(
                  text:" USD",
                  style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white,fontSize: 14,fontWeight: FontWeight.normal),
                )
              ]
          ),
        ),
      ),
    );
  }

  Widget widgetDashboard(){
    return Container(
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
        )
      ),
      child: ListView(
        padding: EdgeInsets.only(top: 20),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
           WidgetPopular(),
           WidgetRecommendation(),
           WidgetSMS(),
        ],
      ),
    );
  }

  greater(){

    if(DateTime.now().hour >= 12 && DateTime.now().hour < 17){
      return "Good Afternoon";
    }
    else  if(DateTime.now().hour >= 17 && DateTime.now().hour < 23){
      return "Good Evening";
    }
    else  if(DateTime.now().hour < 12){
      return "Good Morning";
    }
    else {
      return "Hi";
    }
  }
}
