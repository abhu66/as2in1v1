import 'package:grpc/grpc.dart';

//ennum
enum StateChange  {SUCCESS,FAILURE,LOADING}

class GrpcClientSingleton {
  ClientChannel client;
  static final GrpcClientSingleton _singleton = new GrpcClientSingleton._internal();

  factory GrpcClientSingleton() => _singleton;

  GrpcClientSingleton._internal() {
    client = ClientChannel("172.25.139.40", // Your IP here, localhost might not work.
        port: 9090,
        options: ChannelOptions(
          //TODO: Change to secure with server certificates
          credentials: ChannelCredentials.insecure(),
          idleTimeout: Duration(minutes: 1),
        ));
  }

}

