///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use userDescriptor instead')
const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'userid', '3': 1, '4': 1, '5': 9, '10': 'userid'},
    const {'1': 'firstname', '3': 2, '4': 1, '5': 9, '10': 'firstname'},
    const {'1': 'lastname', '3': 3, '4': 1, '5': 9, '10': 'lastname'},
    const {'1': 'address', '3': 4, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'phone', '3': 5, '4': 1, '5': 9, '10': 'phone'},
  ],
};

/// Descriptor for `User`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userDescriptor = $convert.base64Decode('CgRVc2VyEhYKBnVzZXJpZBgBIAEoCVIGdXNlcmlkEhwKCWZpcnN0bmFtZRgCIAEoCVIJZmlyc3RuYW1lEhoKCGxhc3RuYW1lGAMgASgJUghsYXN0bmFtZRIYCgdhZGRyZXNzGAQgASgJUgdhZGRyZXNzEhQKBXBob25lGAUgASgJUgVwaG9uZQ==');
@$core.Deprecated('Use allRequestDescriptor instead')
const AllRequest$json = const {
  '1': 'AllRequest',
};

/// Descriptor for `AllRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List allRequestDescriptor = $convert.base64Decode('CgpBbGxSZXF1ZXN0');
@$core.Deprecated('Use allResponseDescriptor instead')
const AllResponse$json = const {
  '1': 'AllResponse',
  '2': const [
    const {'1': 'user', '3': 1, '4': 3, '5': 11, '6': '.com.grpc.test.proto.User', '10': 'user'},
  ],
};

/// Descriptor for `AllResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List allResponseDescriptor = $convert.base64Decode('CgtBbGxSZXNwb25zZRItCgR1c2VyGAEgAygLMhkuY29tLmdycGMudGVzdC5wcm90by5Vc2VyUgR1c2Vy');
