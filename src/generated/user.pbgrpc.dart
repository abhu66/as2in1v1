///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'user.pb.dart' as $0;
export 'user.pb.dart';

class UserServiceClient extends $grpc.Client {
  static final _$getUser = $grpc.ClientMethod<$0.User, $0.User>(
      '/com.grpc.test.proto.UserService/getUser',
      ($0.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));
  static final _$save = $grpc.ClientMethod<$0.User, $0.User>(
      '/com.grpc.test.proto.UserService/save',
      ($0.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));
  static final _$all = $grpc.ClientMethod<$0.AllRequest, $0.User>(
      '/com.grpc.test.proto.UserService/all',
      ($0.AllRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));

  UserServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.User> getUser($0.User request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getUser, request, options: options);
  }

  $grpc.ResponseFuture<$0.User> save($0.User request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$save, request, options: options);
  }

  $grpc.ResponseStream<$0.User> all($0.AllRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$all, $async.Stream.fromIterable([request]), options: options);
  }
}

abstract class UserServiceBase extends $grpc.Service {
  $core.String get $name => 'com.grpc.test.proto.UserService';

  UserServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.User, $0.User>(
        'getUser',
        getUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.User.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.User, $0.User>(
        'save',
        save_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.User.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AllRequest, $0.User>(
        'all',
        all_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.AllRequest.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
  }

  $async.Future<$0.User> getUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.User> request) async {
    return getUser(call, await request);
  }

  $async.Future<$0.User> save_Pre(
      $grpc.ServiceCall call, $async.Future<$0.User> request) async {
    return save(call, await request);
  }

  $async.Stream<$0.User> all_Pre(
      $grpc.ServiceCall call, $async.Future<$0.AllRequest> request) async* {
    yield* all(call, await request);
  }

  $async.Future<$0.User> getUser($grpc.ServiceCall call, $0.User request);
  $async.Future<$0.User> save($grpc.ServiceCall call, $0.User request);
  $async.Stream<$0.User> all($grpc.ServiceCall call, $0.AllRequest request);
}
