import 'dart:developer';

import 'package:as2in/src/const/colors_const.dart';
import 'package:as2in/src/main/screens/conntact_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetBottomSheetTransferDataPackage extends StatefulWidget {
  const WidgetBottomSheetTransferDataPackage({Key key}) : super(key: key);

  @override
  _WidgetBottomSheetTransferDataPackageState createState() => _WidgetBottomSheetTransferDataPackageState();
}

class _WidgetBottomSheetTransferDataPackageState extends State<WidgetBottomSheetTransferDataPackage> {
  final GlobalKey<FormState> _formInput        = GlobalKey<FormState>();
  TextEditingController _phoneNumberController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: [
        Row(
          mainAxisAlignment:MainAxisAlignment.spaceAround,
          children: [
            Flexible(
              child: Container(
                padding: EdgeInsets.only(left: 30,right: 30,top: 5),
                child: widgetInputFeld(
                    label: "",
                    controller: _phoneNumberController,
                    hint: "",
                    textInputType: TextInputType.number,
                    decoration: decoration(
                        hint:"",
                        label:"Destination Number",
                        asset:""
                    )
                ),
              ),
            ),
            InkWell(
                onTap: (){
                  showModalBottomSheet(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30),
                            topLeft: Radius.circular(30)
                        ),
                      ),
                      backgroundColor: Colors.white,
                      isScrollControlled: true,
                      context: context, builder: (BuildContext context){
                    return Container(
                        height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
                        child: ListView(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          children: [
                            InkWell(
                              child: Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Spacer(),
                                    RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                          text:"Contacts",
                                          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 18)
                                      ),
                                    ),
                                    Spacer(),
                                    Icon(Icons.clear,size: 35,)
                                  ],
                                ),
                                margin: EdgeInsets.all(20),
                              ),
                              onTap: (){
                                Navigator.of(context).pop();
                              },
                            ),
                          ContactsScreen(),
                          ],
                        ));
                  }
                  ).then((value) {
                    setState(() {
                      log("asdas value : $value");
                      this._phoneNumberController.text = value;
                    });
                  });
                },
                child: Image.asset("assets/images/ic_book.png",width: 35,height: 35,)),
            SizedBox(width: 30,)
          ],
        ),
        Padding(
            padding: EdgeInsets.only(left: 20,right: 20),
            child: Divider()),
        Padding(
          padding: EdgeInsets.only(left: 20),
          child: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text:"eg. 08xxxxxxxxxx or 628xxxxxxxx",
                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 10,color: Colors.grey)
            ),
          ),
        ),

      ],
    );
  }

  Widget widgetInputFeld({String label,String field, TextInputType textInputType,String hint, InputDecoration decoration,TextEditingController controller}) {
    return TextFormField(
      controller: controller,
      cursorColor: HexColor(COLOR_NON_ACTIVE),
      style: TextStyle(
          fontFamily: 'Poppins-Regular',
          fontSize: 18,
          fontWeight: FontWeight.w300),
      keyboardType: textInputType,
      // validator: (value) {
      //   return Validators().validateFieldGeneral(label: label,value: value);
      // },
      onChanged: (value){
        setState((){
          if(this._phoneNumberController.value.text.isEmpty){

          }
          else{
          }

        });
      },
      decoration: decoration,
    );
  }


  InputDecoration decoration({String label,String hint,String asset}){
    return InputDecoration(
      labelText: label,
      labelStyle: TextStyle(
          fontFamily: 'Poppins-Regular',
          color: HexColor(COLOR_NON_ACTIVE),
          fontSize: 18,
          fontWeight: FontWeight.w300),
      hintText: hint,
      enabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      border: InputBorder.none,
      // prefixIcon: Padding(
      //     padding: EdgeInsets.only(right: 5, bottom: 0, top: 0),
      //     child: Image.asset(
      //       asset,
      //       scale: 4,
      //     )),
    );
  }
}



class Choice {
  const Choice({this.title, this.icon});
  final String title;
  final String icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Transfer Balance', icon: "assets/images/purchase/balance_transfer_v2.png"),
  const Choice(title: 'Transfer Data Package', icon: "assets/images/purchase/transfer_data_package.png"),
  const Choice(title: 'Transfer to Malaysia', icon: "assets/images/purchase/transfer_to_malaysia.png"),
  const Choice(title: 'PLN Prepaid', icon: "assets/images/purchase/PLN_prepaid.png"),
  const Choice(title: 'Voice Package', icon: "assets/images/purchase/voice_package.png"),
  const Choice(title: 'Alfamart Voucher', icon: "assets/images/purchase/alfamart_v2.png"),
  const Choice(title: 'Indomaret Voucher', icon: "assets/images/purchase/indomaret_v2.png"),
  const Choice(title: 'SMS Package', icon:"assets/images/purchase/sms_package.png"),
  const Choice(title: 'Caller Ring Back Tone', icon:"assets/images/purchase/caller_ring_back_tone.png"),
  const Choice(title: 'As2in1 App Number', icon: "assets/images/purchase/as2in1_app_number.png"),
  const Choice(title: 'Wifi in Indonesia', icon: "assets/images/purchase/wifi.png"),
  const Choice(title: 'DDHK-Infaq', icon: "assets/images/purchase/DDHK_infaq.png"),
  const Choice(title: 'Subscribe P3K', icon: "assets/images/purchase/subscribe_p3k.png"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({Key key, this.choice}) : super(key: key);
  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.headline1.copyWith(fontSize: 12.0,);
    return Container(
      padding: EdgeInsets.all(5),
        child: Center(child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              Image.asset(choice.icon,width: 40,height: 40),
              SizedBox(height: 10,),
              Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
              SizedBox(height: 10,),
            ]
        ),
        )
    );
  }
}
