import 'package:as2in/src/splash/screen/splash_screen.dart';
import 'package:dropdown_banner/dropdown_banner.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final navigatorKey = GlobalKey<NavigatorState>();
    return MaterialApp(
      builder: (context, child) => MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true), child: child),

      title: 'As2in1',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
        textTheme: TextTheme(
          headline1: TextStyle(color: Colors.black,fontSize: 35,fontWeight: FontWeight.w700)
        )
      ),
      home:SplashScreen() ,
      // home: DropdownBanner(
      //     child: SplashScreen(), navigatorKey: navigatorKey)

    );
  }
}